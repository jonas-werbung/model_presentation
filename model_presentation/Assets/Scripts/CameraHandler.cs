﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    // Start is called before the first frame update
    private ObjectHandler _oh;
    private GameObject _cameraModel;
    [SerializeField] public AnimationCurve _curveCameraProgress;

    Transform _mainCam;
    public Transform _rotPoint;
    private float _fRotPointDistance;
    public float _moveToActiveSpeed;

    private Transform _transformTo;

    private Vector3     _v3CamTransitionFrom = new Vector3();
    private Quaternion  _quCamTransitionFrom = new Quaternion();
    private Vector3     _v3LookAtFrom = new Vector3();
    private Vector3     _v3LookUpFrom = new Vector3();

    private Vector3     _v3RotPointTransitionFrom = new Vector3();
    private Vector3     _v3RotPointTransitionTo = new Vector3();

    private float _transisionTime = 1.0f;

    public float _zoomSpeed;
    public float _rotSpeed;
    public float _moveSpeed;
    private float _transisionTimeProgress = 0;
    private float minCamDist = 0.5f;

    //private bool _bTransitionOverflow = false;
    private bool _bFirstUbdate = true;

    public float _fAngle = 60;


    private bool _bfreeCam = true;
    private bool _bCamTransision = false;
    private bool _bRotPointTransition = false;

    private bool _bInteractable = false;

    private void Awake()
    {
        _mainCam = Camera.main.transform;
        TryGetComponent(out _oh);
    }

    // Update is called once per frame
    void Update()
    {
        if (_bCamTransision)
        {
            moveCamToActive();
        }
        else
        {
            if(!_bfreeCam)
                updateCamModleMovment();
        }
    }
    private void updateCamModleMovment()
    {
        if (_bFirstUbdate)
        {
            _bFirstUbdate = false;
            _fRotPointDistance = (_mainCam.position - _rotPoint.position).magnitude;
        }

        if (Camera.main == null || _cameraModel == null)
            return;
        _mainCam.position = _cameraModel.transform.position;
        _mainCam.rotation = _cameraModel.transform.rotation;
        _mainCam.localScale = _cameraModel.transform.lossyScale;
        _rotPoint.position = _mainCam.position + (_mainCam.forward * _fRotPointDistance);
    }
    private void setUpFrreCam()
    {
        //todo: rotPoint nearest to Center of GameObject?
        _rotPoint.position = _mainCam.position + (_mainCam.forward * _fRotPointDistance);
        _bfreeCam = true;
        _bCamTransision = false;
    }
    //change Camera
    public void setModelCameraActive(GameObject cam, float time = Constants.CAM_TRANSITION_TIME, bool interactable = false)
    {
        //check for Camera
        if (cam == null || !cam.TryGetComponent<Camera>(out Camera cams))
            return;
        if (!_bCamTransision && !_bfreeCam && _cameraModel == cam)
            return;

        //set new Camera
        _bfreeCam = false;
        _bInteractable = interactable;
        _cameraModel = cam;

        _fRotPointDistance = (_mainCam.position - _rotPoint.position).magnitude;

        //check for hard cut
        if (time <= 0 || (_mainCam.position == cam.transform.position && _mainCam.rotation == cam.transform.rotation))
        {
            _mainCam.position = cam.transform.position;
            _mainCam.rotation = cam.transform.rotation;
            return;
        }

        //prepare transition
        _transisionTime = time;
        _bCamTransision = true;

        _v3CamTransitionFrom = _mainCam.position;
        _quCamTransitionFrom = _mainCam.rotation;

        _transformTo = cam.transform;

        _v3LookAtFrom = _mainCam.position + _mainCam.forward;
        _v3LookUpFrom = _mainCam.position + _mainCam.up;

        _bRotPointTransition = true;
        _transisionTimeProgress = 0;
    }
    private void moveCamToActive()
    {
        _transisionTimeProgress = _transisionTimeProgress + (Time.deltaTime / _transisionTime);

        //calculate curveProgress
        float curveProgress = _curveCameraProgress.Evaluate(_transisionTimeProgress / _transisionTime);

        if (curveProgress >= 1)
        {
            _bCamTransision = false;
            _transisionTimeProgress = 0.0f;
            if (_bfreeCam)
            {
                _mainCam.position = _transformTo.position;
                _mainCam.rotation = _transformTo.rotation;
            }
            else
                updateCamModleMovment();
            return;
        }
        //translation
        _mainCam.position = Vector3.Lerp(_v3CamTransitionFrom, _transformTo.position, curveProgress);

        //rotation 
        Vector3 lookAt = Vector3.Lerp(_v3LookAtFrom, _transformTo.position + _transformTo.forward, curveProgress);  
        Vector3 lookUp = Vector3.Lerp(_v3LookUpFrom, _transformTo.position + _transformTo.up, curveProgress);       

        _mainCam.rotation = Quaternion.LookRotation(lookAt - _mainCam.position, lookUp - _mainCam.position);

        _rotPoint.position = _mainCam.position + (_mainCam.forward * _fRotPointDistance);
    }
    //free Camera Transformations
    public bool moveFreeCamera(Vector2 p_movement)
    {
        if (!_bInteractable || (p_movement.x == 0 && p_movement.y == 0))
            return false;
        if (!_bfreeCam)
            setUpFrreCam();
        _bCamTransision = false;
        _bfreeCam = true;

        float camSacle = Vector3.Distance(_mainCam.position, _rotPoint.transform.position) * 0.18f;

        Vector3 translation = (_mainCam.right * p_movement.x * _moveSpeed * Time.deltaTime * (-1) * camSacle) +
            (_mainCam.up * p_movement.y * _moveSpeed * Time.deltaTime * (-1) * camSacle);

        _mainCam.Translate(translation, Space.World);
        _rotPoint.Translate(translation, Space.World);


        return true;
    }
    public bool rotateFreeCamera(Vector2 p_rotationValue)
    {
        if (!_bInteractable || (p_rotationValue.x == 0 && p_rotationValue.y == 0))
            return false;
        if (!_bfreeCam)
            setUpFrreCam();

        _bfreeCam = true;
        _mainCam.RotateAround(_rotPoint.position, Vector3.up, p_rotationValue.x * _rotSpeed * Time.deltaTime);
        _mainCam.RotateAround(_rotPoint.position, _mainCam.right , -p_rotationValue.y * _rotSpeed * Time.deltaTime);

        _bCamTransision = false;
        _bfreeCam = true;

        return true;
    }
    public bool zoomFreeCamera(float p_fValue)
    {
        if (!_bInteractable || p_fValue == 0)
            return false;
        if (!_bfreeCam)
            setUpFrreCam();

        _bfreeCam = true;

        float dist = (_rotPoint.position - _mainCam.position).magnitude;
        float moveDist = p_fValue * _zoomSpeed * Time.deltaTime;

        if (moveDist > 0 && dist - moveDist < minCamDist)
        {
            if (dist < minCamDist)
            {
                _mainCam.Translate(Vector3.forward * (minCamDist - dist), Space.Self);
            }
            return false;
        }
        _mainCam.Translate(Vector3.forward * moveDist, Space.Self);

        _bCamTransision = false;
        _bfreeCam = true;

        return true;
    }
    //focus on Objekt
    public void DebugDrawBounds(Vector3 v3Center, Vector3 v3Extents, float p_duration = 3)
    {
        Color color = Color.green;

        Vector3 v3FrontTopLeft;
        Vector3 v3FrontTopRight;
        Vector3 v3FrontBottomLeft;
        Vector3 v3FrontBottomRight;
        Vector3 v3BackTopLeft;
        Vector3 v3BackTopRight;
        Vector3 v3BackBottomLeft;
        Vector3 v3BackBottomRight;

        v3FrontTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top left corner
        v3FrontTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top right corner
        v3FrontBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom left corner
        v3FrontBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom right corner
        v3BackTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top left corner
        v3BackTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top right corner
        v3BackBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom left corner
        v3BackBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner

        v3FrontTopLeft = transform.TransformPoint(v3FrontTopLeft);
        v3FrontTopRight = transform.TransformPoint(v3FrontTopRight);
        v3FrontBottomLeft = transform.TransformPoint(v3FrontBottomLeft);
        v3FrontBottomRight = transform.TransformPoint(v3FrontBottomRight);
        v3BackTopLeft = transform.TransformPoint(v3BackTopLeft);
        v3BackTopRight = transform.TransformPoint(v3BackTopRight);
        v3BackBottomLeft = transform.TransformPoint(v3BackBottomLeft);
        v3BackBottomRight = transform.TransformPoint(v3BackBottomRight);

        Debug.DrawLine(v3FrontTopLeft, v3FrontTopRight, color, p_duration);
        Debug.DrawLine(v3FrontTopRight, v3FrontBottomRight, color, p_duration);
        Debug.DrawLine(v3FrontBottomRight, v3FrontBottomLeft, color, p_duration);
        Debug.DrawLine(v3FrontBottomLeft, v3FrontTopLeft, color, p_duration);

        Debug.DrawLine(v3BackTopLeft, v3BackTopRight, color, p_duration);
        Debug.DrawLine(v3BackTopRight, v3BackBottomRight, color, p_duration);
        Debug.DrawLine(v3BackBottomRight, v3BackBottomLeft, color, p_duration);
        Debug.DrawLine(v3BackBottomLeft, v3BackTopLeft, color, p_duration);

        Debug.DrawLine(v3FrontTopLeft, v3BackTopLeft, color, p_duration);
        Debug.DrawLine(v3FrontTopRight, v3BackTopRight, color, p_duration);
        Debug.DrawLine(v3FrontBottomRight, v3BackBottomRight, color, p_duration);
        Debug.DrawLine(v3FrontBottomLeft, v3BackBottomLeft, color, p_duration);
    }
    public float calculateDistance(float p_FOV, float p_sizeOfObjekt)
    {
        return p_sizeOfObjekt / (2 * Mathf.Tan(p_FOV));
    }
    public void FocusCameraOnGameObject(Vector3 v3Center, Vector3 v3Extents)
    {
        /*
        if (!_bfreeCam)
            return;

        Camera cam = Camera.main;
        GameObject go = _oh.getActiveGameObjekt();

        //save positions/rotatioos vor transisions
        _v3CamTransitionFrom = cam.transform.position;
        _quCamTransitionFrom = cam.transform.rotation;
        _v3RotPointTransitionFrom = _rotPoint.position;

        DebugDrawBounds(v3Center, v3Extents);
        //get the smaller fov (horizontal, vertical)
        float horizontalFOV = 2f * Mathf.Atan(Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad / 2f) * cam.aspect) * Mathf.Rad2Deg;
        float fov = Math.Min(horizontalFOV, cam.fieldOfView);
        //calculate needed distance between Camera and bounds center
        float newDist = calculateDistance(fov, v3Extents.magnitude);
        
        cam.transform.Translate(v3Center - _v3RotPointTransitionFrom, Space.World);
        cam.transform.LookAt(v3Center);

        float oldDist = (v3Center - cam.transform.position).magnitude;
        cam.transform.Translate(cam.transform.forward * (oldDist-newDist), Space.World);

        //save new camera transvom for transition
        _v3CamTransitionTo = cam.transform.position;
        _quCamTransitionTo = cam.transform.rotation;
        _v3RotPointTransitionTo = v3Center;

        //reset camera transform for transition
        cam.transform.position = _v3CamTransitionFrom;
        cam.transform.rotation = _quCamTransitionFrom;
        //set booleans for transition
        _bCamTransision = true;
        _bRotPointTransition = true;
        */
        return;
    }
}