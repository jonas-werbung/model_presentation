﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public enum FixedAxeis
{
    None,
    X,
    Y,
    Z
}
public enum ColorType
{
    NormalColor,
    HighlightedColor,
    PressedColor,
    SelectedColor,
    DisabledColor
}
struct stopAnimation
{
    public Animation animation;
    public int waitFrame;
    public stopAnimation(Animation anim, int wait)
    {
        foreach(AnimationState state in anim)
        {
        }

        animation = anim;
        waitFrame = wait;
    }
    public bool stop()
    {
        if (waitFrame-- <= 0)
        {
            animation.Stop();
            return true;
        }
        return false;
    }
}
struct PlayingAnimation
{
    public ObjectHandler _oh;
    public Animation animation;
    public string name;
    public float time;
    public PlayingAnimation(ObjectHandler oh, Animation animation, string name)
    {
        _oh = oh;
        this.animation = animation;
        this.name = name;
        switch (animation[name].wrapMode)
        {
            case WrapMode.Once:
            default:
                time = animation[name].length;
                break;
            case WrapMode.PingPong:
                time = 2 * animation[name].length;
                break;
        }
        
    }
    public bool testStoping()
    {
        if (animation.IsPlaying(name) && (animation[name].time >= time))
        {
            _oh.stopAnimation(animation);
            return true;
        }
        return false;
    }
}
struct pausedAnimation
{
    public Animation animation;
    public string clipName;

    public pausedAnimation(Animation anim, string clipName)
    {
        this.clipName = clipName;
        animation = anim;
        pauseAnimation();
    }
    public void pauseAnimation()
    {
        animation[clipName].speed = 0;
    }
    public void continueAnimation()
    {
        foreach (AnimationState state in animation)
            state.speed = 1;
    }
}

public class ObjectHandler : MonoBehaviour
{
    private CameraHandler _ch;
    //private InputHandler _ih;

    //private List<Button3D> _l3DButtons = new List<Button3D>();
    private List<stopAnimation> _lStopAnimations = new List<stopAnimation>();
    private List<pausedAnimation> _lPausedAnimations = new List<pausedAnimation>();
    private List<PlayingAnimation> _lPlayingAnimation = new List<PlayingAnimation>();

    //functions
    public void Awake()
    {
        TryGetComponent(out _ch);
        //TryGetComponent(out _ih);
    }
    public void Start()
    {
        deactivateAllCams();
    }
    public void Update()
    {
        tryStopAnimations();
        tryPlayingAnimation();

    }
    private void tryPlayingAnimation()
    {
        if (_lPlayingAnimation.Count <= 0)
            return;
        for (int i = _lPlayingAnimation.Count - 1; i >= 0; i--)
        {
            if (_lPlayingAnimation[i].testStoping())
            {
                _lPlayingAnimation.RemoveAt(i);
            }
        }
    }
    private void tryStopAnimations()
    {
        if (_lStopAnimations.Count <= 0)
            return;
        for (int i = _lStopAnimations.Count - 1; i >= 0; i--)
        {
            if (_lStopAnimations[i].stop())
                _lStopAnimations.RemoveAt(i);
        }
    }
    public GameObject getClone(GameObject p_go)
    {
        return Instantiate(p_go);
    }
    /*
    public void setInputActive(bool p_active = true)
    {
        _ih.setActive(p_active);
    }
    */
    public void rotateImgToCam(GameObject image, FixedAxeis fixedAxis = FixedAxeis.Y)
    {
        GameObject obj = Camera.main.gameObject;
        var lookPos = image.transform.position - obj.transform.position;

        switch (fixedAxis)
        {
            default:
                break;
            case FixedAxeis.X:
                lookPos.x = 0;
                break;
            case FixedAxeis.Y:
                lookPos.y = 0;
                break;
            case FixedAxeis.Z:
                lookPos.z = 0;
                break;
        }
        image.transform.rotation = Quaternion.LookRotation(lookPos);
        return;
    }

    #region Animation
    public bool playAnimation(Animation animation, AnimationClip clip, float offset, OffsetType offsetType)
    {
        continueAnimation(animation);
        if (animation.GetClip(clip.name) == null)
            animation.AddClip(clip, clip.name);
        if (animation.Play(clip.name))
        {
            switch (offsetType)
            {
                default:
                case OffsetType.Time:
                    animation[clip.name].time = offset;
                    break;
                case OffsetType.Frame:
                    animation[clip.name].normalizedTime = offset / (clip.frameRate * clip.length);
                    break;
                case OffsetType.Relative:
                    animation[clip.name].normalizedTime = offset;
                    break;
            }
            _lPlayingAnimation.Add(new PlayingAnimation(this, animation, clip.name));
            return true;

        }
        return false;
    }
    public void playAnimationQueue(Animation animation, List<AnimationClip> clipList)
    {
        continueAnimation(animation);
        if (clipList == null || clipList.Count < 1)
            return;
        bool playFirst = true;
        for (int i = 0; i < clipList.Count; i++)
        {
            if (clipList[i] == null)
                continue;
            if (playFirst)
            {
                if (!animation.GetClip(clipList[i].name))
                    animation.AddClip(clipList[i], clipList[i].name);
                animation.Play(clipList[i].name);
                playFirst = false;
            }
            else
            {
                if (!animation.GetClip(clipList[i].name))
                    animation.AddClip(clipList[i], clipList[i].name);
                animation.PlayQueued(clipList[i].name);
            }
        }
    }
    public void pauseAnimation(Animation animation)
    {
        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                state.speed = 0;
                return;
                _lPausedAnimations.Add(new pausedAnimation(animation, state.name));
                return;
            }
        }
    }
    public bool continueAnimation(Animation animation)
    {
        foreach (AnimationState state in animation)
            state.speed = 1;

        return true;

        if (_lPausedAnimations.Count <= 0)
            return false;

        for (int i = _lPausedAnimations.Count - 1; i >= 0; i--)
        {
            if (_lPausedAnimations[i].animation == animation)
            {
                _lPausedAnimations[i].continueAnimation();
                _lPausedAnimations.RemoveAt(i--);
                return true;
            }
        }
        return false;
    }
    public void stopAnimation(Animation animation, bool resetTime = true)
    {
        continueAnimation(animation);

        if (resetTime)
        {
            foreach (AnimationState state in animation)
            {
                if (animation.IsPlaying(state.name))
                {
                    state.time = 0f;
                    state.speed = 0f;
                }
            }
            _lStopAnimations.Add(new stopAnimation(animation, 2));
        }
        else
            _lStopAnimations.Add(new stopAnimation(animation, 0));
    }
    public bool setAnimationTime(Animation animation, float offset, OffsetType type)
    {
        if (!animation.isPlaying)
            return false;

        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                switch (type)
                {
                    default:
                    case OffsetType.Time:
                        state.time = offset;
                        break;
                    case OffsetType.Relative:
                        state.normalizedTime = (offset / (state.clip.frameRate * state.length));
                        break;
                    case OffsetType.Frame:
                        state.normalizedTime = offset;
                        break;
                }
                return true;
            }
        }
        return false;
    }
    public float getAnimationTime(Animation animation)
    {
        if (!animation.isPlaying)
            return 0;

        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                float time = state.time;
                while (time > state.length)
                    time -= state.length;
                return time;
            }
        }
        return 0;
    }
    public float getAnimationLegth(Animation animation)
    {
        if (!animation.isPlaying)
            return 0;
        //AnimationState state = new AnimationState();
        //foreach (ref AnimationState state in animation) Error CS1510
        foreach (AnimationState state in animation)
        {
            if (animation.IsPlaying(state.name))
            {
                return state.length;
            }
        }
        return 0;
    }
    #endregion

    #region Camera
    private void deactivateAllCams()
    {
        for (int i = Camera.allCamerasCount - 1; i >= 0; i--)
        {
            if (Camera.main != Camera.allCameras[i])
                Camera.allCameras[i].gameObject.SetActive(false);
            else
                Camera.allCameras[i].gameObject.SetActive(true);
        }
        Camera.main.gameObject.SetActive(true);
        Debug.Log("--");
    }
    public void changeCamera(GameObject camera, float time = 1, bool interactable = false)
    {
        _ch.setModelCameraActive(camera, time, interactable);
    }
    void FocusCamOnAvtiveModel()
    {
        /*
        Vector3 center;
        Vector3 expanse;
        if (_lModels[getActiveModelIndex()].getAnimation().isPlaying)
        {
            center = getActiveGameObjekt().transform.position;
            expanse = getActiveBounds().extents;
            center.y += expanse.y / 2;
        }
        else
        {
            center = getActiveGameObjekt().transform.position;
            expanse = getActiveBounds().extents;
            center.y += expanse.y / 2;
        }
        _ch.FocusCameraOnGameObject(center, expanse);
        */
    }
    public void transformCamera2D(float moveX, float moveY, float rotateX, float rotateY, float zoom)
    {
        transformCamera2D(new Vector2(moveX, moveY), new Vector2(rotateX, rotateY), zoom);
    }
    public void transformCamera2D(Vector2 moveCamera, Vector2 rotateCamera, float zoomCamera)
    {
        _ch.moveFreeCamera(moveCamera);
        _ch.rotateFreeCamera(rotateCamera);
        _ch.zoomFreeCamera(zoomCamera);
    }
    #endregion

    #region colorBlock
    void AssignIfNotNull<T>(T newValue, ref T receiver) where T : class
    {
        receiver = newValue ?? receiver;
    }
    private void GetPropertyValues(object obj)
    {
        Type t = obj.GetType();
        Debug.Log("Type is: " + t.Name);
        PropertyInfo[] props = t.GetProperties();
        Debug.Log("Properties (N = " + props.Length + "):");

        int i = 0;

        foreach (var prop in props)
        {
            if (prop.GetIndexParameters().Length == 0)
                Debug.Log("(" + i++ + ")   " + prop.Name + " (" + prop.PropertyType.Name + "): " + prop.GetValue(obj));
            else
                Debug.Log("(" + i++ + ")   " + prop.Name + " (" + prop.PropertyType.Name + "): <Indexed>");
        }
        Debug.Log("End of Type " + t.Name + "____________________________________________________________________________________________________________________");
    }
    public void changeColorBlock(GameObject obj, Color color, ColorType type)
    {
        Component[] com = obj.GetComponents(typeof(Component));

        for (int i = 0; i < com.Length; i++)
        {
            var v = com[i].GetType().GetProperty("colors");
            if (v == null)
                continue;
            ColorBlock colors = (ColorBlock)v.GetValue(com[i]);

            switch (type)
            {
                case ColorType.NormalColor:
                    colors.normalColor = color;
                    break;
                case ColorType.HighlightedColor:
                    colors.highlightedColor = color;
                    break;
                case ColorType.PressedColor:
                    colors.pressedColor = color;
                    break;
                case ColorType.SelectedColor:
                    colors.selectedColor = color;
                    break;
                case ColorType.DisabledColor:
                    colors.disabledColor = color;
                    break;
            }
            v.SetValue(com[i], colors);
        }
    }

    /*
    public RectTransform create3DBtn(string p_name)
    {
        GameObject go = getClone((GameObject)Resources.Load(Constants.PATH_3D_BUTTON_PREFAB));
        GameObject canvas = GameObject.FindGameObjectWithTag(Constants.CANVAS_HIGHLIGHT_NAME);
        if (go == null || canvas == null)
        {
            return null;
        }
        go.transform.SetParent(canvas.transform);
        go.name = "3D_btn_" + p_name;

        if (go.TryGetComponent<RectTransform>(out RectTransform rect))
            return rect;
        GameObject.Destroy(go);
        return null;
    }
    public bool add3DButton(GameObject model, string name)
    {
        if (model == null || name.Length <= 0)
            return false;
        Button3D btn = new Button3D();
        
        btn.name = name;
        btn.transform = model.transform;
        btn.screenTransform = create3DBtn(name);

        _l3DButtons.Add(btn);
        return true;
    }
    public string test3DButtonPosition(Vector3 screenPosition)
    {
        bool found = false;
        int index = 0;
        for (int i = 0; i < _l3DButtons.Count; i++)
        {
            if((screenPosition - _l3DButtons[i].screenTransform.position).magnitude < Constants.DISTANCE_TRIGGER_2D_FAR)
            {
                index = i;
                if (found && Vector3MagnitudeSqrt(screenPosition - _l3DButtons[i].screenTransform.position) < Vector3MagnitudeSqrt(screenPosition - _l3DButtons[index].screenTransform.position))
                    index = i;
                found = true;
            }
        }
        if (found)
        {
            return _l3DButtons[index].name;
        }
        return null;
    }
    public float Vector3MagnitudeSqrt(Vector3 vector)
    {
        return vector.x * vector.y * vector.z;
    }
    public void update3DButtonPositions()
    {
        for(int i = 0; i < _l3DButtons.Count; i++)
        {
            _l3DButtons[i].screenTransform.position = Camera.main.WorldToScreenPoint(_l3DButtons[i].transform.position);
            float camDist = (_l3DButtons[i].transform.position - Camera.main.transform.position).magnitude;
            float scale = camDist * Constants.SCALE_3D_BUTTON_MOD;
            _l3DButtons[i].screenTransform.localScale = Vector3.one * Math.Min(Math.Max(scale, Constants.SCALE_3D_BUTTON_MIN), Constants.SCALE_3D_BUTTON_MAX);
        }
    }
    */
    #endregion

    #region touch
    public float getTouchPinch(int fingerCount)
    {
        if (Input.touchCount == fingerCount)
        {
            Vector2 touch0 = new Vector2();
            for (int i = 0; i < fingerCount; i++)
            {

                touch0 += Input.GetTouch(0).deltaPosition;
            }
            return (touch0 / fingerCount).magnitude;
        }
        else
            return 0f;
    }
    public float getTouchPinch()
    {
        if (Input.touchCount < 2)
            return 0;

        Vector2 midpoint = new Vector2();
        float distanceNow = 0;
        float distanceThen = 0;
        for (int i = 0; i < Input.touchCount; i++)
        {
            midpoint += Input.GetTouch(i).position;
        }
        midpoint = midpoint / Input.touchCount;

        for (int i = 0; i < Input.touchCount; i++)
        {
            distanceNow += (Input.GetTouch(i).position - midpoint).magnitude;
            distanceThen += ((Input.GetTouch(i).position - Input.GetTouch(i).deltaPosition) - midpoint).magnitude;
        }

        float t = (float)Math.Sqrt(Screen.currentResolution.height * Screen.currentResolution.height + Screen.currentResolution.width * Screen.currentResolution.width);

        return (distanceNow - distanceThen) / t;
    }
    #endregion
}


