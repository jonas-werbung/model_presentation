﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingAnimation
{

    public string _animName1;
    public string _animName2;
    public float _time;
    public string _message;

    public LoadingAnimation(string p_name1, string p_name2, string message, float p_time)
    {
        _animName1 = p_name1;
        _animName2 = p_name2;
        _time = p_time;
        _message = message;
    }
}
public enum OffsetType 
{
    Time,
    Relative,
    Frame
}
public enum Trigger
{
    Button,
    Objekt,
    Position
}
public enum PlayPauseMod
{
    Toggel,
    Pause,
    Play
}
public struct AnimationSave
{
    public string name;
    public float time;
    public bool isSaved;
}
public struct Button3D
{
    public string name;
    public Transform transform;
    public RectTransform screenTransform;
}
public static class Constants
{
    //IDs
    public const int POSITION_TRIGGER_TYP_3DBOTTON      = 0;
    public const int POSITION_TRIGGER_TYP_MESH          = 1;
    public const int POSITION_TRIGGER_TYP_POSITION      = 2;
    public const int POSITION_TRIGGER_TYP_INFO_TEXT     = 2;
    public const int ANIMATION_TYP_SIPMLE               = 3;
    public const int ANIMATION_TYP_ROW                  = 4;
    public const int ANIMATION_TYP_CHARGE               = 5;
    public const int ANIMATION_TYP_3D_BUTTON            = 6;

    //constants nummer
    public const float DISTANCE_TRIGGER_2D_FAR          = 30;
    public const float DISTANCE_TRIGGER_2D_SHORT        = 10;
    public const float SCALE_3D_BUTTON_MAX              = 0.5f;
    public const float SCALE_3D_BUTTON_MIN              = 0.1f;
    public const float SCALE_3D_BUTTON_MOD              = 0.03f;

    public const float CAM_TRANSITION_TIME              = 1.0f;

    //constants string
    public const string CANVAS_HIGHLIGHT_NAME           = "canvasHighlight";
    public const string CANVAS_MAIN_TAG                 = "canvasMain";
    public const string PATH_BUTTON_PREFAB              = "2D/Button";
    public const string PATH_3D_BUTTON_PREFAB           = "2D/3DButton";
    public const string INFO_TEXT_STD_BTN_NAME          = "InfoText";

    //color
    public static Color BTN_COLOR_ACTTIVE               = Color.yellow;
    public static Color BTN_COLOR_INACTIVE              = Color.white;
}
