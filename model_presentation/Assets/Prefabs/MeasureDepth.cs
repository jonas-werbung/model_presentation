﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class MeasureDepth : MonoBehaviour
{
    public MultiSourceManager _multiSource;
    public Texture2D _depthTexture;

    int t = 0;

    // Cutoffs
    [Range(0,5.0f)]
    public float _depthSensitivity = 5;

    [Range(-10, 10f)]
    public float _wallDepth = -10;

    [Header("Top and Bottom")]
    [Range(-2, 2)]
    public float _cutoffTop = -1.5f;
    [Range(-2, 2)]
    public float _cutoffBotton = 1.5f;

    [Header("Left and Right")]
    [Range(-2, 0)]
    public float _cutoffLeft = -1.5f;
    [Range(0, 2)]
    public float _cutoffRight = 1.5f;

    // Depth
    private ushort[] _depthData = null;
    private CameraSpacePoint[] _camSapecePoints = null;
    private ColorSpacePoint[] _colorSpacePoints = null;
    List<ValidPoint> _validPoints = null;

    // Kinect
    private KinectSensor _sensor = null;
    private CoordinateMapper _mapper = null;
    private Camera _mainCamera;

    private readonly Vector2Int _depthResolution = new Vector2Int(512, 424);
    private Rect _rect;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _sensor = KinectSensor.GetDefault();
        _mapper = _sensor.CoordinateMapper;

        int arraySize = _depthResolution.x * _depthResolution.y;

        _camSapecePoints = new CameraSpacePoint[arraySize];
        _colorSpacePoints = new ColorSpacePoint[arraySize];

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        //if(--t <= 0)
        {
            _validPoints = depthToColor();

            _rect = CreateRect(_validPoints);

            _depthTexture = createTexture(_validPoints);
            t = 100;
        }
    }
    private void OnGUI()
    {
        GUI.Box(_rect, "");
    }

    private List<ValidPoint> depthToColor()
    {
        int count = 0;
        int countTested = 0;
        int count3 = 0;
        int countL = 0;
        int countR = 0;
        int countT = 0;
        int countB = 0;

        // Points to return
        List<ValidPoint> validPoints = new List<ValidPoint>();

        // Get depth
        _depthData = _multiSource.GetDepthData();

        // Map
        _mapper.MapDepthFrameToCameraSpace(_depthData, _camSapecePoints);
        _mapper.MapDepthFrameToColorSpace(_depthData, _colorSpacePoints);

        // Filter
        for (int x = 0; x < _depthResolution.x; x++)
        {
            for (int y = 0; y < _depthResolution.y; y++)
            {
                // SapmleIndex
                int sampleIndex = (x * _depthResolution.y) + y;
                //sampleIndex *= 8;

                countTested++;

                if (float.IsInfinity(_camSapecePoints[sampleIndex].X) && float.IsInfinity(_camSapecePoints[sampleIndex].Y) && float.IsInfinity(_camSapecePoints[sampleIndex].Z))
                {
                    count3++;
                    continue;
                }

                // Cutoff tests
                if (_camSapecePoints[sampleIndex].X < _cutoffLeft)
                {
                    countL++;
                    continue;
                }
                if (_camSapecePoints[sampleIndex].X > _cutoffRight)
                {
                    countR++;
                    continue;
                }
                if (_camSapecePoints[sampleIndex].Y > _cutoffTop)
                {
                    countT++;
                    continue;
                }
                if (_camSapecePoints[sampleIndex].Y < _cutoffBotton)
                {
                    countB++;
                    continue;
                }

                count++;
                ValidPoint newPonit = new ValidPoint(_colorSpacePoints[sampleIndex], _camSapecePoints[sampleIndex].Z);


                // depth test
                if (_camSapecePoints[sampleIndex].Z >= _wallDepth)
                    newPonit._bWithinWallDepth = true;

                validPoints.Add(newPonit);

            }
        }
        Debug.Log("Testted : " + countTested +"_Valid Points: " + count + "_AND Infinity: " + count3);
        Debug.Log("Cuttofs: L - R - T - B: " + countL + " - " + countR + " - " + countT + " - " + countB);
        return validPoints;
    }
    private Texture2D createTexture(List<ValidPoint> p_validPoints)
    {
        Texture2D texture = new Texture2D(1920, 1080, TextureFormat.Alpha8, false);
        for (int x = 0; x < 1920; x++)
        {
            for (int y = 0; y < 1080; y++)
            {
                texture.SetPixel(x, y, Color.clear);
            }
        }

        foreach(ValidPoint cPoint in p_validPoints)
        {
            texture.SetPixel((int)cPoint.colorSpace.X, (int)cPoint.colorSpace.Y, Color.black);
        }

        texture.Apply();

        return texture;
    }

    #region Rect Creation
     private Rect CreateRect(List<ValidPoint> points)
    {
        if (points.Count == 0)
        {
            Debug.Log("0 Valid Points");
            return new Rect();
        }

        // get corners of rect
        Vector2 topLeft = GetTopLeft(points);
        Vector2 bottomRight = GetBottonRight(points);

        // translate to viewport
        //topLeft = ScreenToCamera(topLeft);
        //bottomRight = ScreenToCamera(bottomRight);

        // rect dimensions
        float width = bottomRight.x - topLeft.x;
        float height = bottomRight.y - topLeft.y;

        // create
        Vector2 size = new Vector2(width, height);
        Rect rect = new Rect(topLeft, size);

        return rect;
    }
    private Vector2 GetTopLeft(List<ValidPoint> points)
    {
        Vector2 topLeft = new Vector2(int.MaxValue, int.MaxValue);
        float xMax = 0, xMin = (float)int.MaxValue, yMax = 0, yMin = (float)int.MaxValue;

        foreach (ValidPoint point in points)
        {
            // Left most x
            if (point.colorSpace.X < xMin)
                xMin = point.colorSpace.X;
            if (point.colorSpace.X > xMax)
                xMax = point.colorSpace.X;

            //Top most y
            if (point.colorSpace.Y < yMin)
                yMin = point.colorSpace.Y;
            if (point.colorSpace.Y > yMax)
                yMax = point.colorSpace.Y;
        }

        return new Vector2(xMin, yMin);
    }
    private Vector2 GetBottonRight(List<ValidPoint> points)
    {
        Vector2 bottomRight = new Vector2(0, 0);

        foreach (ValidPoint point in points)
        {
            // Right most x
            if (point.colorSpace.X > bottomRight.x)
                bottomRight.x = point.colorSpace.X;

            // Bottom most y
            if (point.colorSpace.Y > bottomRight.y)
                bottomRight.y = point.colorSpace.Y;
        }

        return bottomRight;
    }
    private Vector2 ScreenToCamera(Vector2 screenPosition)
    {
        Vector2 normalizedScreen = new Vector2(Mathf.InverseLerp(0, 1920, screenPosition.x), Mathf.InverseLerp(0, 1080, screenPosition.y));

        Vector2 screenPoint = new Vector2(normalizedScreen.x * _mainCamera.pixelWidth, normalizedScreen.y * _mainCamera.pixelHeight);

        return screenPoint;
    }
    #endregion
}

public class ValidPoint
{
    public ColorSpacePoint colorSpace;
    public float z = 0.0f;

    public bool _bWithinWallDepth = false;

    public ValidPoint(ColorSpacePoint p_colorSpace, float p_z)
    {
        colorSpace = p_colorSpace;
        z = p_z;
    }
}
